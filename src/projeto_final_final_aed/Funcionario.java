package projeto_final_final_aed;
public class Funcionario {
	String nome;
	long cartaodecidadao;
	int departamento;
	int cargo;
	Funcionario proximo;
    
	public  Funcionario( String nome, long cartaodecidadao , int departamento ,int cargo, Funcionario proximo){
    	
		this.nome=nome; 
		this.cartaodecidadao=cartaodecidadao;
		this.departamento= departamento;
		this.cargo=cargo;
		this.proximo=proximo;
	}
    
	public String ToString(){
    	
		return "Nome : ( " + nome + " ), B.I/C.C : ( " + cartaodecidadao + " )";
	}
    
}
