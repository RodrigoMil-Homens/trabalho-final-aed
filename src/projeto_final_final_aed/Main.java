
package projeto_final_final_aed;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class Main {
	
	
	public static void main(String[] args) throws IOException{
    	
		//DeclaraÃ§ao das variaveis do programa
		
		int[] cargo=new int [15];
		int[] valor_cargo=new int [15];
		int[] soma_de_salarios=new int [15];
		int[] soma_de_salarios_por_departamento = new int [10];
		
		int valor_Maior=0;
		int valor_Menor=0;
		int numero_funcionarios_valor_Maior=0;
		int codigo_departamento_valor_Maior=0;
		int numero_funcionarios_valor_Menor=0;
		int codigo_departamento_valor_Menor=0;
		//int quantas_pe
		
		String ficheiro1 = "funcionarios.txt";
		String ficheiro_Salarios = "salarios.txt";
		String funcionarios_repetidos="";
    
		FileWriter writer = null;

		Lista lista = new Lista();
		
		//_____________________________________________________________________________________________________
    
		//Ã‰ criada uma arvore AVL
		AvlTree t = new AvlTree();
  
		//_____________________________________________________________________________________________________
		
		
		//O codigo seguinte ira abrir o ficheiro funcionarios.txt, e irÃ¡ ler e guardar os funcionarios numa lista dinamica ,
    
		Scanner scan;
		Scanner scanner;
    
		try {
		
			scanner = new Scanner(new BufferedReader( new FileReader(ficheiro1)));
			scan=scanner.useDelimiter("\\s*:\\s*|\\s*\n\\s*");
				
			while(scan.hasNextLine()){
				String nome = scan.next();
				String cartaodecidadao = scan.next();//Este cartao de cidadao e uma string e tera de se tornar um long para que possa ser inserido\pesquisado numa arvore avl
				int departamento = scan.nextInt();
				int cargo_do_funcionario= scan.nextInt();
			
				//A proxima linha parte a string do cartao de cidadao pelo "." e guarda as strings no array cc_sem_pontos[]
				String cc_sem_pontos[] = cartaodecidadao.split(Pattern.quote("."));
				long cartao_de_cidadao_long=0;
			
				//Este try/catch serve para transformar a string do B.I\C.C para um long Uma vez que nao e possivel guarada-lo no int devido ao seu tamanho
				try{
				
					String para_converter="";
				
					//Este for() concatena todas as strings do array cc_sem_pontos[]
					for(int x=0;x<cc_sem_pontos.length;x++){
						para_converter += cc_sem_pontos[x];
					}
				
					//Uma vez que a variavel para_converter jÃ¡ contem a string do B.I\C.C sem "."  iremos agora iremos agora convereter a string num long 
					cartao_de_cidadao_long = Long.parseLong(para_converter);
				
				}catch (NumberFormatException nfe){// Este NumberFormatException serve para que caso exista algum erro na conversÃ£o da string para um long nos seja retornada a mensagem do erro
					
					System.out.println("NumberFormatException: " + nfe.getMessage());
				}
			
			
			
				Funcionario funcionario=new Funcionario(nome,cartao_de_cidadao_long,departamento,cargo_do_funcionario,null);	//Faz-se a instancia do funcionario e passa-se todos os atributos inclusivo o B.I\C.C transformado num long 			
				inserirInicio(lista, funcionario);// Inser no inico da lista dos funcionarios
			
			
				//O proximo if() faz um search() na arvore avl pelo B.I\C.C, e se o search() retornar um null significa que nÃ£o foi encontrada nehum funcionario com esse B.I\C.C
			
				if(t.search(cartao_de_cidadao_long) == null){
				
					t.insert(cartao_de_cidadao_long, funcionario);// Como nÃ£o foi encontrado nehum funcionario com o mesmo B.I\C.C, adiciona na arvore 
				
				}else{
				
					funcionarios_repetidos += ("( " + nome + " )");// Como foi encontrado um funcionario com o mesmo B.I\C.C, concatena na variavel String funcionarios_repetidos o nome do funcionario uma vez que nos Ã© pedido apenas o nome
				}
			
			
				if (scan.hasNextLine()) scan.nextLine();//Esta linha de codigp permite passar para a linha seguinte do ficheiro funcionarios
			}

			//Escreve as 2 linhas seguinte permitem fechar o ficheiro para leitura 
			scan.close();
			scanner.close();

		
		}catch (FileNotFoundException e) {//  O FileNotFoundException serve para returnar uma mensagem de erro caso nÃ£o seja encontrado o ficheiro funcionario.txt
			e.printStackTrace();
			return;
		} 
    
		
		//O codigo seguinte ira abrir/ler o ficheiro salario.txt.
		
		try {
		
			scanner = new Scanner(new BufferedReader( new FileReader(ficheiro_Salarios)));
			scan=scanner.useDelimiter("\\s*:\\s*|\\s*\n\\s*");
				
			while(scan.hasNextLine()){
	
				int cargo_do_salario = scan.nextInt();
				int valor_do_salario= scan.nextInt();
			
				switch(cargo_do_salario){//para tornar o codigo mais rapido possivel iremos guardar no array de 15 elementos "uma vez que nos e dado no enunciado a informaÃ§Ã£o que apenas existem 15 cargos diferentes", para que a complexidade da pesquisa do valor do cargo seja igual a 1 "seja direto" 
				
					case 1:
						
						valor_cargo[0]=valor_do_salario;// Os cargos que estÃ£o no ficheiro salarios estÃ£o de 1 a 15, entao o valor_do_salario e passado para o array valor_cargo[]
						break;
						
					case 2:
						
						valor_cargo[1]=valor_do_salario;
						break;
						
					case 3:
						
						valor_cargo[2]=valor_do_salario;
						break;
						
					case 4:
						
						valor_cargo[3]=valor_do_salario;
						break;
						
					case 5:
						
						valor_cargo[4]=valor_do_salario;
						break;
						
					case 6:
						
						valor_cargo[5]=valor_do_salario;
						break;
						
					case 7:
						valor_cargo[6]=valor_do_salario;
						break;
						
					case 8:
						
						valor_cargo[7]=valor_do_salario;
						break;
						
					case 9:
						
						valor_cargo[8]=valor_do_salario;
						break;
						
					case 10:
						
						valor_cargo[9]=valor_do_salario;
						break;
						
					case 11:
						
						valor_cargo[10]=valor_do_salario;
						break;
						
					case 12:
						
						valor_cargo[11]=valor_do_salario;
						break;  
						
					case 13:
						
						valor_cargo[12]=valor_do_salario;
						break;
						
					case 14:
						valor_cargo[13]=valor_do_salario;
						break;
							
					case 15:
						
						valor_cargo[14]=valor_do_salario;
						break;
						
					default:
						
						System.out.println("existe um erro, no ficheiro ha um cargo errado");
                
				}
			
				if (scan.hasNextLine()) scan.nextLine();
			}

			scan.close();
			scanner.close();
		
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} 
    

		//A partir deste momento temos a arvore avl implementada e balanceada, temos uma lista com todos os funcionarios, temos um array  que a partir do numero do cargo retorna o valor do salario  
		//Para provar que a arvore avl esta bem implementada e balanceada basta fazer o displayTree(); ou o search pelo um B.I/C.C 
		// As 3 linhas de codigo seguintes estÃ£o documentadas, e provam que estÃ¡ tudo bem

		
		//t.displayTree();
		//Funcionario teste = t.search(11441344129268L);
		//System.out.println(teste.ToString());


		if(lista.inicio!=null){ //iremos percorrer a lista dos funcionarios 
			Funcionario func = lista.inicio;
    		
			while(func != null){//Enquanto existir um funcionario faz:
    		
				soma_de_salarios_por_departamento[(func.departamento-1)] += valor_cargo[func.cargo-1];//Soma os salarios por departamento 
       
        
				switch(func.cargo){//obtem o cargo do funcionario, e com esse cargo faz um switch() case:.
					case 1:
                	
						cargo[0]++;//O primeiro elemento de um qualquer array comeÃ§a em 0 e acaba em length-1, neste caso vai guardar no array a quantidade de funcionarios que tem como cargo o valor 1 
						soma_de_salarios[0]+= valor_cargo[0];//Nesta linha de codigo  soma o salario de todos os funcionarios que tenham o cargo igual a 1
                    
						break;
                
					case 2:
                	
						cargo[1]++;
						soma_de_salarios[1]+= valor_cargo[1];

						break;
                    
					case 3: 
						
						cargo[2]++;
						soma_de_salarios[2]+= valor_cargo[2];
                  
						break;
                    
					case 4: 
						
						cargo[3]++;
						soma_de_salarios[3]+= valor_cargo[3];
                    

						break;
						
					case 5:  
						
						cargo[4]++;
						soma_de_salarios[4]+= valor_cargo[4];

						break;
						
					case 6: 
						
						cargo[5]++;
						soma_de_salarios[5]+= valor_cargo[5];

						break;    
                    
					case 7:  
						
						cargo[6]++;
						soma_de_salarios[6]+= valor_cargo[6];
                    
						break; 
						
					case 8:  
						
						cargo[7]++;
						soma_de_salarios[7]+= valor_cargo[7];

						break;
						
					case 9:  
						
						cargo[8]++;
						soma_de_salarios[8]+= valor_cargo[8];

						break;
						
					case 10:  
						
						cargo[9]++;
						soma_de_salarios[9]+= valor_cargo[9];

						break;  
						
					case 11: 
						
						cargo[10]++;
						soma_de_salarios[10] += valor_cargo[10];
  
						break;
						
					case 12:  
						
						cargo[11]++;
						soma_de_salarios[11]+=valor_cargo[11];

						break;
						
					case 13:  
						
						cargo[12]++;
						soma_de_salarios[12]+= valor_cargo[12];
                    
						break;
						
					case 14:
						
						cargo[13]++;
						soma_de_salarios[13]+= valor_cargo[13];

						break;
						
					case 15:  
						
						cargo[14]++;
						soma_de_salarios[14]+= valor_cargo[14];

						break;
                    
					default:
						System.out.println("o numero de cargo nao existe");
				}
            
				func = func.proximo;
			}
    	
    	
		}else{
			System.out.println("Lista vazia");
		}
    
    

		//Os metodos get_Maior_e_Menor(), e o metodo get_codigo_departamento_Maior_e_Menor() sao uma modificaÃ§Ã£o do alguritmo do bubllesort.
		//Este metodo foi otpimizado para percorrer apenas uma unica vez, encontra o elemento do array do valor_cargo[] que corresponde ao valor 
		//do salaÌ�rio mais alto que a empresa paga actualmente,
		//Cria um objeto Objecto_para_respostas e passa o valor do salario mais alto que a empresa paga atualmente, e passa tambem o endereÃ§o que corresponde ao cargo 
		//para que depois possa encontrar o numero de todos os funcionarios que tem o salario mais alto.
		
		
		Objecto_para_respostas objecto_para_maiores = get_Maior_e_Menor(valor_cargo,1);//passa o valor 1 que significa que vai encontrar o maior 
		
		valor_Maior=objecto_para_maiores.getValor_maior();//acede ao objeto objecto_para_maiores e obtem o valor do salario mais alto que a empresa paga atualmente
		numero_funcionarios_valor_Maior=cargo[objecto_para_maiores.getCargo_do_valor_maior()];//acede ao objeto objecto_para_maiores e obtem o endereÃ§o que corresponde ao cargo, e a partir desse endereÃ§o descobre o numero de funcionarios que tem esse salario
    
		
		Objecto_para_respostas objecto_para_menores = get_Maior_e_Menor(valor_cargo,0);//passa o valor 0 que significa que vai encontrar o menor
		
		valor_Menor=objecto_para_menores.getValor_menor();
		numero_funcionarios_valor_Menor=cargo[objecto_para_menores.getCargo_do_valor_Menor()];
		

		//Objecto_para_respostas objecto_para_maiores_por_departamento = get_codigo_departamento_Maior_e_Menor(soma_de_salarios_por_departamento,1);
		
		//int valor_Maior_por_departamento=objecto_para_maiores.getValor_maior();//acede ao objeto objecto_para_maiores e obtem o valor do salario mais alto que a empresa paga atualmente
		//int numero_funcionarios_valor_Maior=cargo[objecto_para_maiores.getCargo_do_valor_maior()];//acede ao objeto objecto_para_maiores e obtem o endereÃ§o que corresponde ao cargo, e a partir desse endereÃ§o descobre o numero de funcionarios que tem esse salario
    
		codigo_departamento_valor_Maior = get_codigo_departamento_Maior_e_Menor(soma_de_salarios_por_departamento,1);// Esta linha de codigo permite da a resposta a pergunta 5 do enunciado
		codigo_departamento_valor_Menor = get_codigo_departamento_Maior_e_Menor(soma_de_salarios_por_departamento,0);// Esta linha de codigo permite da a resposta a pergunta 7 do enunciado
    

    
    
		//Escrever no ficheiro as resposta!!
    
		File logFile=null;
		
		try {
			//obtem a date em que foi compilado,cria um novo ficheiro de output com as respostas,e atribui a data ao nome do ficheiro
			String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			logFile = new File(timeLog);  

			logFile.createNewFile();
        
			writer = new FileWriter(logFile,true);
        
			writer.write("--1 Qual o salÃ¡rio da pessoa com o BI / CC = 5904234569? ==> " + valor_cargo[t.search(5904234569L).cargo-1] + "\n\n");
			writer.write("--2 Qual o total de salaÌ�rios das pessoas do Departamento com coÌ�digo = 10?\n");
			writer.write("1 => " + soma_de_salarios_por_departamento[0] + "\n");
			writer.write("2 => " + soma_de_salarios_por_departamento[1] + "\n");
			writer.write("3 => " + soma_de_salarios_por_departamento[2] + "\n");
			writer.write("4 => " + soma_de_salarios_por_departamento[3] + "\n");
			writer.write("5 => " + soma_de_salarios_por_departamento[4] + "\n");
			writer.write("6 => " + soma_de_salarios_por_departamento[5] + "\n");
			writer.write("7 => " + soma_de_salarios_por_departamento[6] + "\n");
			writer.write("8 => " + soma_de_salarios_por_departamento[7] + "\n");
			writer.write("9 => " + soma_de_salarios_por_departamento[8] + "\n");
			writer.write("10 => " + soma_de_salarios_por_departamento[9] + "\n\n");
			writer.write("--3 Qual o total de salaÌ�rios das pessoas com o Cargo com coÌ�digo = 3? ==> " + soma_de_salarios[2] + "\n\n");
			writer.write("--4 Qual eÌ� o valor do salaÌ�rio mais alto que a empresa paga actualmente? ==> " + valor_Maior + "\n\n");
			writer.write("--5 Quantas pessoas teÌ‚m o salaÌ�rio mais alto? ==> " + valor_Maior + ", " + numero_funcionarios_valor_Maior + " pessoas\n\n" );
			writer.write("--6 Qual eÌ� o valor do salaÌ�rio mais baixo que a empresa paga actualmente? ==> " + valor_Menor + "\n\n");
			writer.write("--7 Quantas pessoas teÌ‚m o salaÌ�rio mais baixo? ==> " + valor_Menor + ", " + numero_funcionarios_valor_Menor + " pessoas\n\n");
			writer.write("--8 Qual o coÌ�digo do departamento com o total de salaÌ�rios (pagos actualmente) mais alto? ==> Departamento " + (codigo_departamento_valor_Maior+1) + " com um total de salÃ¡rios " + soma_de_salarios_por_departamento[codigo_departamento_valor_Maior] + "\n\n");
			writer.write("--9 Qual o coÌ�digo do departamento com o total de salaÌ�rios (pagos actualmente) mais baixo? ==> Departamento " + (codigo_departamento_valor_Menor+1) + " com um total de salÃ¡rios " + soma_de_salarios_por_departamento[codigo_departamento_valor_Menor] + "\n\n");
			writer.write("--10 Identificar as pessoas (BI/CC) que aparecem repetidas no ficheiro1 ==> " + funcionarios_repetidos);
        
        
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
    
	
	
	private static Objecto_para_respostas get_Maior_e_Menor(int[] array,int op) {
		
		//copia o array que foi passado para essa funÃ§ao que foi passado para o seguinte elemento
		int[] array_copy = Arrays.copyOf(array, 15);        
		int endereco=0;

		if(op==1){//se a opÃ§Ã£o for 1 significa que e para encontrar o maior
			
			for (int i = 0; i < array_copy.length; i++) {  //percorre cada elemeto 
				if (array_copy[i] > array_copy[endereco]) {  // se o elemento que esta a ser percorrido for maior que o elemento que esta dado como maior , entÃ£o o endereÃ§o toma o valor da posiÃ§ao atual "i"
					endereco=i;
					
				}
			}

			Objecto_para_respostas objecto_para_maiores = new Objecto_para_respostas();

			objecto_para_maiores.setValor_maior(array_copy[endereco]);// passa o salario mais alto que a empresa esta a pagar para o objeto que foi acabado de criar 
			objecto_para_maiores.setCargo_do_valor_maior(endereco);//passa o endereÃ§o que corresponde ao cargo 

			return objecto_para_maiores;
        
		}else{//Caso contrario encontra o menor e faz o mesmo processo do maior para o menor
        	
			for (int i = 0; i < array_copy.length; i++) {                                       
				if (array_copy[i] < array_copy[endereco]) {  
        			endereco=i;
    			
				}
			}

			Objecto_para_respostas objecto_para_menores = new Objecto_para_respostas();

			objecto_para_menores.setValor_menor(array_copy[endereco]);
			objecto_para_menores.setCargo_do_valor_Menor(endereco);

			return objecto_para_menores;
		}


	}
    
	//a funÃ§ao seguinte ira receber o array com a soma  do departamento e ira receber um int 0 ou 1 quera identigicar o maior\menor
	
	  private static int get_codigo_departamento_Maior_e_Menor(int[] array,int op) {
			
	    	int[] array_copy = Arrays.copyOf(array, 10);       
	        int departamento=0;


	        if(op==1){
	        	for (int i = 0; i < array_copy.length-1; i++) { 
	        		
	        		if (array_copy[i] > array_copy[departamento]) {  
	        			departamento=i;
	        	
	        		}	
	        	}

	        	return departamento;
	        
	        }else{
	        	departamento=0;
	        	for (int i = 0; i < array_copy.length-1; i++) {                                       
	        		if (array_copy[i] < array_copy[departamento]) {  
	        			departamento=i;
	    
	        		}
	        		
	        	}

	        	return departamento;
	        }

	    }
	
    
	public static void inserirInicio(Lista lista, Funcionario funcionario){
		Funcionario t = funcionario;
		if (lista.inicio==null && lista.fim==null) {
			//lista vazia
			lista.inicio=t;
			lista.fim=t;
		} else {
			//lista tem pelo menos um elemento
			t.proximo=lista.inicio;
			lista.inicio=t;
		}
	}
	
	public static void listar(Lista lista){
		if (lista.inicio!=null) {
			
			Funcionario t = lista.inicio;
			while (t != null) {
				System.out.println(t.toString());
				t = t.proximo;
			}
		} else {
			System.out.println("Lista vazia");
		}
	}
	
}
