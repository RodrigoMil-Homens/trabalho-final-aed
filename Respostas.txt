--1  Qual o salário da pessoa com o BI / CC = 650.121.2004? ==> 8300

--2 Qual o total de salários das pessoas do Departamento com código = 10?
1 =>	4400
2 =>	25008
3 =>	26500
4 =>	8200
5 =>	345000
6 =>	10000
7 =>	340000
8 =>	58000
9 =>	57008
10 =>	20308

--3 Qual o total de salários das pessoas com o Cargo com código = 3? ==> 174300

--4 Qual é o valor do salário mais alto da empresa? ==> 24000

--5 Quantas pessoas têm o salário mais alto?  ==> 24000, 1 pessoa

--6 Qual é o valor do salário mais baixo da empresa? ==> 3100

--7 Quantas pessoas têm o salário mais baixo?  ==> 3100, 5 pessoas

--8 Qual o código do departamento com o total de salários mais alto?  ==> Departametno 80 com total de salários 345000

--9 Qual o código do departamento com o total de salários mais baixo? ==> Departamento 10 com 4400

--10 Identificar as pessoas (BI/CC) que aparecem repetidas no ficheiro1 ==> David Austin